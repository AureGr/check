package annuaire.om;

import java.io.Serializable;

public class Personne  implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String nom;
	private String prenom;
	private int    age;
	
	public Personne() {
		this.nom    = "";
		this.prenom = "";
		this.age    = 0;
	}
	public Personne(String nom, String prenom, int age) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
	}
	public String toString() {
		return this.nom+" "+this.prenom+" ("+this.age+")";
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

}
