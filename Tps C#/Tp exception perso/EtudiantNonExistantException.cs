﻿using System;
using System.Collections.Generic;
using System.Text;

namespace cTp5
{
    class EtudiantNonExistantException : Exception
    {
        private int numero;

        public EtudiantNonExistantException(int n)
        {
            this.numero = n;
        }

        public override string Message
        {
            get
            {
                return "L'etudiant de numéro : " + numero + " n'existe pas!";
            }
        }

    
    }
}
