package annuaire.iu;

import java.util.Enumeration;

import annuaire.om.Annuaire;
import annuaire.om.Personne;

/**
 * @author aureg
 * blyat reponses aux questions
 * Combien le projet présente-t-il de package ? 
 * 3
 * Et de classes ?
 * 3
 *
 * Combien y a t’il de classes comportant une méthode main() ?
 * 1
 * 
 * A quoi servent chaque classe ?
 * Main : compile le projet, intencie des objet des classes et affiche le tout : création d'un annuaire, création des personnes et ajout dans annuaire, affichage
 * 
 * Personne (class java bin = constructeur default, attributs privés, get/set) : objet personne avec attribut et overwrite tostring
 * Annuaire : objet annuaire liste personnes (collectionneur) , get, remove, add...
 * 
 **/

public class Main {
	
	public static void afficherAnnuaire(Annuaire annuaire) {
		
		Enumeration<Personne>   enuPersonne = annuaire.getPersonnes();
		
		System.out.println("------------------------------------------------------");
		System.out.println("l'annuaire ["+annuaire.getNom()+"] contient "+annuaire.size()+" personne(s)");
		System.out.println("------------------------------------------------------");
		while (enuPersonne.hasMoreElements()) {
			System.out.println( enuPersonne.nextElement().toString() );
		}
	}
	
	public static void main(String[] args) {
		
		Personne   personne;
		Annuaire   annuaire;
		
		annuaire = new Annuaire("Fans de Java");
		
		personne = new Personne("DUPOND","Jean", 42);
		annuaire.add(personne);
		personne = new Personne("DURAND","Pierre", 24);
		annuaire.add(personne);
		personne = new Personne("MOULIN","Marie", 50);
		annuaire.add(personne);
		personne = new Personne("O'BRIEN","Damien", 22);
		annuaire.add(personne);
		
		afficherAnnuaire(annuaire);
		
		Personne jean = new Personne("BOND","Jean", 55);
		annuaire.add(jean);
		personne = new Personne("PEUPLU","Jean", 31);
		annuaire.add(personne);

		afficherAnnuaire(annuaire);
		if (annuaire.contains(jean)) {
			System.out.println("JEAN B est dans l'annuaire");
		} else {
			System.out.println("JEAN B n'est pas dans l'annuaire");
		}
		
		annuaire.remove(jean);
		

		afficherAnnuaire(annuaire);
		if (annuaire.contains(jean)) {
			System.out.println("JEAN B est dans l'annuaire");
		} else {
			System.out.println("JEAN B n'est pas dans l'annuaire");
		}

		annuaire.add(jean);

		System.out.println("=======================================================");
		System.out.println("Personnes ayant le prénom ''Jean'' :");
		for ( Enumeration<Personne> enuPersonne = annuaire.getPersonnesFromPrenom("Jean"); enuPersonne.hasMoreElements(); ) {
			System.out.println( enuPersonne.nextElement().toString() );
		}
		
		System.out.println("=======================================================");
		System.out.println("Personnes ayant le prénom ''Michel'' :");
		for ( Enumeration<Personne> enuPersonne = annuaire.getPersonnesFromPrenom("Michel"); enuPersonne.hasMoreElements(); ) {
			System.out.println( enuPersonne.nextElement().toString() );
		}
		System.out.println("=======================================================");
		System.out.println("Liste des prénoms de l'annuaire :");
		for ( Enumeration<String> enuPrenom = annuaire.getPrenoms(); enuPrenom.hasMoreElements(); ) {
			System.out.println( enuPrenom.nextElement() );
		}

		System.out.println("====================  F  I  N  ========================");
		//afficherAnnuaire(annuaire);

		
	}

}
