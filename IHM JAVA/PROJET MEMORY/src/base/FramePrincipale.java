package base;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;

import memory.om.Jeu;

@SuppressWarnings("serial")
public class FramePrincipale extends JFrame  {
	private Jeu jeu;
	private Grille grille;
	private panelMenu panelMenu;
	private int nbPaire				=8;
	private boolean modeTriche		=false;
	
	public FramePrincipale() {
		super();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(700,500));

		panelMenu = new panelMenu(this);
		jeu = new Jeu(nbPaire);
		grille = new Grille(this);
		
		
		this.setLayout(new BorderLayout());
		this.add(grille, BorderLayout.CENTER);


		this.add(panelMenu, BorderLayout.WEST);
		this.setJMenuBar(new MenuBar(this));

		this.pack();







	}

	
	
	
	public void retry() {
		reset(jeu);
	}
	
	public void newGame() {
		jeu=new Jeu(nbPaire, modeTriche);
		reset(jeu);
		
	}
	
		
	
	public void setScore() {
		panelMenu.setScore(grille.getNbCoupsJoues());
	}
	

	public void reset(Jeu jeu) {
		grille.setNbCoupsJoues(0);
		this.remove(grille);
		grille= new Grille(this);
		this.add(grille, BorderLayout.CENTER);
		panelMenu.setScore(0);
		this.pack();
		
	}
	
	public int getNbCoups() {
		return grille.getNbCoupsJoues();
	}
	
	public boolean isPartieLancee() {
		return grille.isPartieLancee();
	}
	
	public Jeu getJeu() {
		return this.jeu;
	}
	
	public void setNbPaires(int pfNbPaires) {
		this.nbPaire=pfNbPaires;
	}
	
	public panelMenu getPanelMenu() {
		return this.panelMenu;
	}
	
	
	
	public void setTriche(boolean triche) {
		modeTriche=triche;
		newGame();
	}
	
	
	


}
