
from PIL import Image

#methode fonctionnelle -> steganoImageLast ligne 105

#test manip en vrac
def Testbit(nimg):
  imageHote = Image.open(nimg)
  longueur, hauteur = imageHote.size
  print(longueur, hauteur)
  p1 = imageHote.getpixel((500, 500))
  print(p1)
  p2 = (80, 1, 15)
  
  
  p2=list(p2)
  print(p2)
  p3=[bin(x)[2:].rjust(8,"0")for x in p1]
  print(p3)
  print(p3[1][7])

  
  

#exemple cacher un message
def stegano(name_img , msg):
    im = Image.open(name_img)
    # on récupère les dimensions de l'image
    w , h = im.size

    # on sépare l'image en trois : rouge, vert et bleu
    r , g , b = im.split()

    # on transforme la partie rouge en liste
    r = list( r.getdata() )

    # on calcule la longueur de la chaîne et on la transforme en binaire
    u = len(msg)
    v = bin( len(msg) )[2:].rjust(8,"0")

    # on transforme la chaîne en une liste de 0 et de 1 
    ascii = [ bin(ord(x))[2:].rjust(8,"0") for x in msg ]

    # transformation de la liste en chaîne
    im2 = ''.join(ascii)

    # on code la longueur de la liste dans les 8 premiers pixels rouges
    for j in range(8):
        r[j] = 2 * int( r[j] // 2 ) + int( v[j] )

    # on code la chaîne dans les pixels suivants
    for i in range(8*u):
        r[i+8] = 2 * int( r[i+8] // 2 ) + int( im2[i] )
        
    # on recrée l'image rouge 
    nr = Image.new("L",(16*w,16*h))
    nr = Image.new("L",(w,h))
    nr.putdata(r)

    # fusion des trois nouvelles images
    imgnew = Image.merge('RGB',(nr,g,b))
    new_name_img = "couv_" + name_img
    imgnew.save(new_name_img)



#pas fini, je croyais qu'il falait convertir
def steganoI(name_img , img2):
    im = Image.open(name_img)
    im1 = Image.open(img2)
    # on récupère les dimensions de l'image
    w , h = im.size
    w2 , h2 = im1.size

    print(w, h)
    print(w2, h2)

    for W in range(w2):
      for H in range(h2):
        p1=im.getpixel((W,H))
        p2=im1.getpixel((W,H))
        print(p2)
        pb1=[bin(x)[2:].rjust(8,"0")for x in p1]
        pb2=[bin(y)[2:].rjust(8,"0")for y in p2]
        for x in range (3):
          for i, j in zip(range (4), range(5, 8)):
            pb1[x][j]=pb2[x][i]
            print (i, j)
        print(pb1)
   


   

def testsize(ims):
  im=Image.open(ims)
  print(im.size)






#cacher une image dans une autre
#probleme: l'image a cacher ne peut pas etre plus petite que l'autre
#im = image hote
#scredim = image a caher
#--OUT file : melange.png
def steganoImageLast(im, scredim):
   
  im1 = Image.open(im) # image qui restera visible
  im2 = Image.open(scredim) # image à cacher
  l, h = im1.size

  
  resimage = Image.new('RGB', (l, h))

  for x in range(l):
      for y in range(h):
          r1, g1, b1 = im1.getpixel((x, y))
          r1, g1, b1 = r1 & 0b11110000, g1 & 0b11110000, b1 & 0b11110000
          r2, g2, b2 = im2.getpixel((x, y))
          r2, g2, b2 = r2 >> 4, g2 >> 4, b2 >> 4
          resimage.putpixel((x,y), (r1+r2, g1+g2, b1+b2))
      # r1 + r2 = les 4 bits de poids fort de r1initial à gauche,
      # les 4 bits de poids forts de r2initial à droite

  
  resimage.save('melange.png')
 


#trouver une image cachée dans une autre
#OUT file : demelange.png
def steganoDemasquerLast(img):
      
    im = Image.open(img)
    l, h = im.size

    
    im_sortie = Image.new('RGB', (l, h))

    for x in range(l):
        for y in range(h):
            r, g, b = im.getpixel((x, y))
            im_sortie.putpixel((x,y), ((r << 4 ) & 0b11110000, (g << 4 ) & 0b11110000, (b << 4 ) & 0b11110000))


    ##-----Finalisation-----##
    im_sortie.save('demelange.png')
    
    




        