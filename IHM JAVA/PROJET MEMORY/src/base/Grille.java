package base;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.Timer;

import memory.om.Jeu;
import memory.om.Reponse;

@SuppressWarnings("serial")
public class Grille extends JPanel implements ActionListener{
	
	private int nbLig;
	private int nbCol;
	private ArrayList<JButton> allBut = new ArrayList<JButton>();
	private Jeu jeu; 
	private int nbPaire;
	private JButton premiereCase;
	private JButton caseChoisie;
	private FramePrincipale theFrame;
	private boolean perduFini=true;
	private int nbCoupsJoues =0;
	private boolean partieLancee=false ;
	private Font Arial3 = new Font("Arial",Font.BOLD,50);
	private boolean butTrouve ;
	private int index=0 ;
	private Reponse reponse;
	
	public Grille(FramePrincipale pfFrame) {
		super();
		
		theFrame=pfFrame;
		jeu=pfFrame.getJeu();
		nbPaire= jeu.getNbPaires();
		nbLig=(int)(Math.ceil(Math.sqrt(nbPaire*2)));
		nbCol=nbLig;


		this.setLayout(new GridLayout(nbLig, nbCol));
		this.setBackground(Color.WHITE);

		for (int i=0; i<nbPaire*2; i++) {

			JButton but = new JButton("");
			but.addActionListener(this);
			but.setFont(Arial3);
			allBut.add(but);
			this.add(but);

		}




	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		if(perduFini && !jeu.isPartieTerminee()) {
		butTrouve=false;
		for (int i=0; i<allBut.size()*2 && !butTrouve; i++) {
			if (e.getSource()==allBut.get(i)) {
				partieLancee=true;
				theFrame.setScore();
				index = i;
				caseChoisie=allBut.get(i);
				butTrouve=true ;
			}
		}

		caseChoisie.setText(""+jeu.getCarteValeur(index));
		caseChoisie.setEnabled(false);
		reponse = jeu.jouer(index);

		if (reponse==Reponse.PREMIERE) {
			premiereCase=caseChoisie;
			nbCoupsJoues++;
		}
		else if (reponse==Reponse.PERDU) {
			
			perduFini=false;
			Timer timer = new Timer(350, new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					premiereCase.setEnabled(true);
					premiereCase.setText("");
					caseChoisie.setEnabled(true);
					caseChoisie.setText("");
					perduFini=true;
					
				}
			} );

			timer.setRepeats(false);
			timer.start();
		}

		else if (reponse==Reponse.GAGNE) {
			premiereCase=null;
                                                                                                                                                                                                                                                 

		}
	}
	}
	
public boolean isPartieLancee() {
	return partieLancee;
}
	
public int getNbCoupsJoues() {
	return this.nbCoupsJoues;
}
public void setNbCoupsJoues(int val) {
	this.nbCoupsJoues=val;
}

}



