<?php
namespace FilmothequeBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Entity
*/
class Film {

  /**
  * @ORM\GeneratedValue
  * @ORM\Id
  * @ORM\Column(type="integer")
  */
  private $id;
  
  /**
  * @ORM\Column(type="string", length=255)
  * Assert\NotBlank()
  * @Assert\Length(
    min = 3,
    minMessage = "Le titre de film doit contenir au moins {{ limit }} characters ",
  * )
  */
  private $titre;
  
  /**
  * @ORM\Column(type="string", length=500)
  */
  private $description;
  
  /**
  * @ORM\JoinColumn(nullable = false)
  * @Assert\NotBlank()
  * @ORM\ManyToOne(targetEntity="Categorie")
  */
  private $categorie;
  
  /**
  * @ORM\ManyToMany(targetEntity="Acteur")
  */
  private $acteurs;
  
  
  
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->acteurs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Film
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Film
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set categorie
     *
     * @param \FilmothequeBundle\Entity\Categorie $categorie
     *
     * @return Film
     */
    public function setCategorie(\FilmothequeBundle\Entity\Categorie $categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \FilmothequeBundle\Entity\Categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Add acteur
     *
     * @param \FilmothequeBundle\Entity\Acteur $acteur
     *
     * @return Film
     */
    public function addActeur(\FilmothequeBundle\Entity\Acteur $acteur)
    {
        $this->acteurs[] = $acteur;

        return $this;
    }

    /**
     * Remove acteur
     *
     * @param \FilmothequeBundle\Entity\Acteur $acteur
     */
    public function removeActeur(\FilmothequeBundle\Entity\Acteur $acteur)
    {
        $this->acteurs->removeElement($acteur);
    }

    /**
     * Get acteurs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActeurs()
    {
        return $this->acteurs;
    }
}
