<?php

namespace FilmothequeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FilmothequeBundle\Form\FilmType;
use Symfony\Component\HttpFoundation\Request;

use FilmothequeBundle\Entity\Film;

class FilmController extends Controller {


  public function listerAction(){
   
   $em = $this->getDoctrine()->getManager();
   $films = $em->getRepository('FilmothequeBundle:Film')->findAll();
   return $this->render('@Filmotheque/Film/lister.html.twig',array('films' =>$films));
   
 }
 public function getAction($id=null){
   
    $em  = $this->getDoctrine()->getManager();
    $film = $em->find('FilmothequeBundle:Film', $id);
	return $this->render('@Filmotheque/Film/voir.html.twig', array('film'=>$film));
	
 }
 
    public function supprimerAction(Request $request, $id){
    
      $em = $this->getDoctrine()->getManager();
      $film = $em->find('FilmothequeBundle:Film',$id);
      if(!$film){
        throw new NotFoundHttpException("film non trouve");
      }
      $em->remove($film);
      $em->flush();
      return $this->redirectToRoute('filmotheque_film_lister');
    
    }
    
    
 public function editerAction(Request $request, $id=null) {
		$em  = $this->getDoctrine()->getManager();
		
		if (isset($id)) {
			$film = $em->find('FilmothequeBundle:Film', $id);
		} else {
			$film = new Film();
		}
		
		$formfilm = $this->createForm(filmType::class, $film);
		
		if ($request->isMethod('POST')  && $formfilm->handleRequest($request)->isValid()) {
			$em->persist($film);
			$em->flush();
			
			$request->getSession()->getFlashBag()->add('notice', 'film enregistre.');
			
			return $this->redirectToRoute('filmotheque_film_lister');
		}
		
		return $this->render('@Filmotheque/Film/editer.html.twig', array('formFilm' => $formfilm->CreateView ()));
	}
	
	public function topAction($max=3){
		$em=$this->getDoctrine()->getManager();
		$qb=$em->createQueryBuilder();
		$qb->select('f')
			->from('FilmothequeBundle:Film','f')
			->orderBy('f.titre', 'ASC')
			->setMaxResults($max);
		$query=$qb->getQuery();
		$films=$query->getResult();
		return $this->render('@Filmotheque/Film/liste.html.twig',array('films'=>$films));
	}

}