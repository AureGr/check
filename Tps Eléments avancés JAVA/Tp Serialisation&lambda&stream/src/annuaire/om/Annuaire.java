package annuaire.om;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.stream.Stream;

/**
 * Annuaire de Personnes
 * @author Fabrice Pelleau
 *
 */
public class Annuaire implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String                                nom;
	private ArrayList<Personne>                   listePersonnes;
	transient HashMap<String, ArrayList<Personne>>  indexPrenoms;
	
	
	/**
	 * Constructeur d'un annuaire
	 * @param nom nom de l'annuaire
	 */
	public Annuaire(String nom) {
		this.nom            = nom;
		this.listePersonnes = new ArrayList<>();
		this.indexPrenoms   = new HashMap<>();
	}
	
	/**
	 * Nom de l'annuaire
	 * @return le nom
	 */
	public String getNom() {
		return this.nom;
	}
	
	/**
	 * Ajout d'une personne dans l'annuaire
	 * @param pers la personne à ajouter
	 */
	public void add(Personne pers) {
		this.listePersonnes.add(pers);
		ArrayList<Personne> lst = this.indexPrenoms.get(pers.getPrenom());
		if (lst==null) {
			lst = new ArrayList<>();
			this.indexPrenoms.put(pers.getPrenom(), lst);
		}
		lst.add(pers);
	}

	/**
	 * Taille de l'annuaire
	 * @return le nombre de personnes enregistrées dans l'annuaire
	 */
	public int size() {
		return this.listePersonnes.size();
	}
	
	/**
	 * Recherche une personne dans l'annuaire
	 * @param pers l'objet Personne à rechercher
	 * @return true si l'objet est déjà dans l'annuaire
	 */
	public boolean contains(Personne pers) {
		return this.listePersonnes.contains(pers);
	}
	
	/**
	 * Supprimer une personne de l'annuaire (première occurrence seulement)
	 * @param pers la personne à supprimer
	 * @return true si la personne a été supprimée
	 */
	public boolean remove(Personne pers) {
		ArrayList<Personne> lst = this.indexPrenoms.get(pers.getPrenom());
		lst.remove(pers);
		if (lst.size()==0) {
			this.indexPrenoms.remove(pers.getPrenom());
		}
		
		return this.listePersonnes.remove(pers);
	}
	
	/**
	 * Contenu complet de l'annuaire
	 * @return enumeration sur toutes les personnes de l'annuaire
	 */
	public Enumeration<Personne> getPersonnes() {
		return Collections.enumeration(this.listePersonnes);
	}
	
	/**
	 * Liste des personnes ayant un prénom donné
	 * @param prenom le prénom à rechercher
	 * @return enumeration sur les personnes trouvées
	 */
	public Enumeration<Personne> getPersonnesFromPrenom(String prenom) {
		
		ArrayList<Personne> listeRecherche = this.indexPrenoms.get(prenom);
		if (listeRecherche==null) {
			return Collections.emptyEnumeration();
		} else {
			return Collections.enumeration(listeRecherche);		
		}
	}
	
	public Enumeration<String> getPrenoms() {
		return Collections.enumeration( this.indexPrenoms.keySet() );
	}
	
	public void writePersonne(String NOM_FIC) {
		try {
			OutputStream flux = new FileOutputStream( NOM_FIC );
			ObjectOutputStream fluxDonnees = new ObjectOutputStream(flux); 
			fluxDonnees.writeInt(listePersonnes.size());
			for (Personne val : listePersonnes) {
				fluxDonnees.writeObject(val);
			}
			
			flux.close();
			System.out.println("Sauvegarde ok" +NOM_FIC);
			
		} catch (FileNotFoundException e) {
			System.err.println("Impossible d'ouvrir le fichier" +NOM_FIC);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println("Impossible d'ecrire le fichier" +NOM_FIC);
		} 
			
		
		
	}
	
	/*public void writePersonnes (OutputStream out) throws IOException{
	 * 	ObjectOutputStream......
	 * }
	 * fileinputstream et try/catch declaré  lors de l'appel
	 * 
	 * same pour read mais avec try/catch 
	 */
	
	public void readPersonne(String NOM_FIC) {
		try {
			InputStream flux = new FileInputStream( NOM_FIC );
			ObjectInputStream fluxDonnees = new ObjectInputStream(flux); 
			int nbItem = fluxDonnees.readInt();
			System.out.println("Il y a "+nbItem+ "valeurs :");
			while (nbItem > 0) {
				Personne unePersonne = (Personne) fluxDonnees.readObject();
				System.out.println(unePersonne.toString());
				this.add(unePersonne);
				nbItem--;
			}
			fluxDonnees.close();
			System.out.println("Restoration ok" ); 	
			
		} catch (FileNotFoundException e) {
			System.err.println("Impossible d'ouvrir le fichier " +NOM_FIC);
		} catch (IOException e) {
			System.err.println("Impossible de lire le fichier " +NOM_FIC);
		} catch (ClassNotFoundException e) {
			System.out.println("CONTENU DU FICHIER INCORRECT ("+e.getMessage()+")");

			
		} 
			
		
		
	}
	
	public void writeAnnuaire(String NOM_FIC) {
		try {
			OutputStream flux = new FileOutputStream( NOM_FIC );
			ObjectOutputStream fluxDonnees = new ObjectOutputStream(flux); 
			
			fluxDonnees.writeObject(this);
			
			
			flux.close();
			System.out.println("Sauvegarde ok" +NOM_FIC);
			
		} catch (FileNotFoundException e) {
			System.err.println("Impossible d'ouvrir le fichier" +NOM_FIC);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println("Impossible d'ecrire le fichier" +NOM_FIC);
		} 
			
		
		
	}
	
	
	public void readAnnuaire(String NOM_FIC) {
		try {
			InputStream flux = new FileInputStream( NOM_FIC );
			ObjectInputStream fluxDonnees = new ObjectInputStream(flux); 
			
			Annuaire unAnnuaire = (Annuaire) fluxDonnees.readObject();
			System.out.println(unAnnuaire.toString());
			this.listePersonnes=unAnnuaire.listePersonnes;
			this.indexPrenoms = new HashMap<>();
			for (Personne p : this.listePersonnes) {
				ArrayList<Personne> lst = this.indexPrenoms.get(p.getPrenom());
				if (lst==null) {
					lst = new ArrayList<>();
					
					this.indexPrenoms.put(p.getPrenom(), lst);
				
				}
				lst.add(p);
			}
			
			fluxDonnees.close();
			System.out.println("Restoration ok" ); 	
			
		} catch (FileNotFoundException e) {
			System.err.println("Impossible d'ouvrir le fichier " +NOM_FIC);
		} catch (IOException e) {
			System.err.println("Impossible de lire le fichier " +NOM_FIC);
		} catch (ClassNotFoundException e) {
			System.out.println("CONTENU DU FICHIER INCORRECT ("+e.getMessage()+")");

			
		} 
			

	
	}
	
	public Stream<Personne> stream(){
		return this.listePersonnes.stream();
	}
	
	
}
