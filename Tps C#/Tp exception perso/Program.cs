﻿using System;
using System.Collections.Generic;

namespace cTp5
{
    class Program
    {
        static void Main(string[] args)
        {
            Universite univ = new Universite(new List<Etudiant>() { new Etudiant(1, DateTime.Parse("16/05/1964")), new Etudiant(2, DateTime.Parse("01/05/1554")), new Etudiant(3, DateTime.Parse("16/11/1999")), new Etudiant(4, DateTime.Parse("20/05/2000")), new Etudiant(5, DateTime.Parse("16/09/1800")), new Etudiant(6, DateTime.Parse("30/06/2001")), new Etudiant(7, DateTime.Parse("16/03/2002")), new Etudiant(8, DateTime.Parse("15/01/1964")), new Etudiant(9, DateTime.Parse("17/08/1950")), new Etudiant(10, DateTime.Parse("25/12/1961")) });

            Etudiant e = univ.getEtudiant(1111);

            Console.Read();

        }
    }
}
