package anno2;

import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic.Kind;

import anno2.NommageMethode.Type;

@SupportedAnnotationTypes({"anno2.NommageMethode"})
public class NommageMethodeProc extends AbstractProcessor {
	private Messager messager;
	
	
	@Override
	public synchronized void init(ProcessingEnvironment processingEnv) {
		super.init(processingEnv);
		this.messager = processingEnv.getMessager();
	}
	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

		System.out.println("Traitement des annotations : DEBUT ");
		for (TypeElement anno : annotations) {
			
			
			for (Element e : roundEnv.getElementsAnnotatedWith(anno)) {
				
				
				if (e.getKind()==ElementKind.METHOD) {
					NommageMethode monAnnotation = e.getAnnotation(NommageMethode.class);
					if (monAnnotation != null) {
						if (monAnnotation.coucou().equals(Type.SHORT)) {
							if(e.getSimpleName().length()<5 || Character.isUpperCase(e.getSimpleName().charAt(0)) )
								this.messager.printMessage(Kind.WARNING , "Nom Methode critique", e);
					
							
						}
						
						if (monAnnotation.coucou().equals(Type.LONG)) {
							if(e.getSimpleName().length()<12 || Character.isUpperCase(e.getSimpleName().charAt(0)) )
								this.messager.printMessage(Kind.WARNING , "Nom Methode critique", e);
							
						}
					}
				}

			}
		}
		
		
		
		System.out.println("Traitement des annotations : FIN ");
		
		return false;
	}

}
