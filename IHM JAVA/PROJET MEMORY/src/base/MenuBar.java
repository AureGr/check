package base;

import java.awt.event.ActionEvent;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButton;

@SuppressWarnings("serial")
public class MenuBar extends JMenuBar{
	
	private JCheckBox triche = new JCheckBox("Triche");
	private JRadioButton mcoup = new JRadioButton("Coups");
	private JRadioButton mtime = new JRadioButton("Temps");
	private ButtonGroup grp = new ButtonGroup();

	private JMenu menu = new JMenu("Menu");
	private JMenu menu2 = new JMenu("Mode");
	private JMenuItem menuNew = new JMenuItem("New");
	private JMenuItem menuRetry = new JMenuItem("Retry");
	
	private FramePrincipale theFrame;
	
	public MenuBar(FramePrincipale pfFrame) {
		super();
		theFrame=pfFrame;
		
		
		this.add(menu);
		menu.add(menuNew);
		menu.add(menuRetry);
		menu.add(menu2);
		menu.add(triche);
		menu2.add(mcoup);
		menu2.add(mtime);
		grp.add(mcoup);
		grp.add(mtime);
		
		
		menuRetry.addActionListener((ActionEvent e) -> theFrame.getPanelMenu().retry());
		menuNew.addActionListener((ActionEvent e) -> theFrame.getPanelMenu().newGame());
		mcoup.addActionListener((ActionEvent e)-> theFrame.getPanelMenu().modeCoup());
		mtime.addActionListener((ActionEvent e)-> theFrame.getPanelMenu().modeTime());
		triche.addActionListener((ActionEvent e)-> theFrame.setTriche(triche.isSelected()));
	}
}
