package anno2;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;


@Target({ElementType.METHOD})
public @interface NommageMethode {
	Type coucou() default Type.SHORT;
	
	public enum Type {
		SHORT, LONG
	}
	
}



