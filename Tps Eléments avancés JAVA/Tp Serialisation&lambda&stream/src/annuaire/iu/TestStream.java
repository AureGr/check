package annuaire.iu;

import java.util.Enumeration;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import annuaire.om.Annuaire;
import annuaire.om.Personne;

public class TestStream {
	public static String NOM_FIC = "listePersonne.sav";
	public static String NOM_FIC_Annuaire = "annuaire.sav";

	public static void main(String[] args) {
		Annuaire   annuaire;
			
		annuaire = new Annuaire(" rest Gens Sauvés");
	
		//annuaire.readPersonne(NOM_FIC);
		annuaire.readAnnuaire(NOM_FIC_Annuaire);
		//afficherAnnuaire(annuaire);
		
		
		System.out.println("=======================================================");
		System.out.println("Personnes ayant le prénom ''Andy'' :");
		for ( Enumeration<Personne> enuPersonne = annuaire.getPersonnesFromPrenom("Andy"); enuPersonne.hasMoreElements(); ) {
			System.out.println( enuPersonne.nextElement().toString() );
		}
		
		System.out.println("==================Stream 1==================");
		annuaire.stream().forEach(p-> System.out.println(p.toString()));
		
		System.out.println("==================Stream 2==================");
		annuaire.stream().filter(p-> p.getAge()>30).forEach(p-> System.out.println(p.toString()));
		
		System.out.println("==================Stream 3==================");
		annuaire.stream().filter(p-> p.getAge()>30 && p.getPrenom().length()<=5).forEach(p-> System.out.println(p.toString()));
		
		System.out.println("==================Stream 4==================");
		List<Personne> ps = annuaire.stream().filter(p-> p.getAge()>30).collect(Collectors.toList());
		System.out.println(ps.toString());
	
		System.out.println("==================Stream 4==================");
		System.out.println(annuaire.stream().filter(p-> p.getPrenom().equals("Jean")).count());
		
		System.out.println("==================Stream 5==================");
		annuaire.stream().findFirst().filter(p-> p.getPrenom().equals("Jean")).ifPresent(System.out::print);
		
		
	
	
	}
	
	
public static void afficherAnnuaire(Annuaire annuaire) {
		
		Enumeration<Personne>   enuPersonne = annuaire.getPersonnes();
		
		System.out.println("------------------------------------------------------");
		System.out.println("l'annuaire ["+annuaire.getNom()+"] contient "+annuaire.size()+" personne(s)");
		System.out.println("------------------------------------------------------");
		while (enuPersonne.hasMoreElements()) {
			System.out.println( enuPersonne.nextElement().toString() );
		}
	}

}

