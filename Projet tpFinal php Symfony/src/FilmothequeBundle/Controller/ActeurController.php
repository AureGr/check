<?php

namespace FilmothequeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FilmothequeBundle\Form\ActeurRechercheType;
use FilmothequeBundle\Form\ActeurType;
use Symfony\Component\HttpFoundation\Request;

use FilmothequeBundle\Entity\Acteur;

class ActeurController extends Controller {
	
	public function listerAction(){
   
   $em = $this->getDoctrine()->getManager();
   $acteurs = $em->getRepository('FilmothequeBundle:Acteur')->findAll();
   $formRechActeur = $this->createForm(ActeurRechercheType::class);
   return $this->render('@Filmotheque/Acteur/lister.html.twig',array('acteurs' =>$acteurs,
										 'formRechActeur' => $formRechActeur->createView()));
   
 }
	
/*	public function ajouterAction(Request $request) {
		$acteur = new Acteur();
		$formActeur = $this->createForm(ActeurType::class, $acteur);
		
		if ($request->isMethod('POST')  && $formActeur->handleRequest($request)->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($acteur);
			$em->flush();
			
			$request->getSession()->getFlashBag()->add('notice', 'Acteur enregistre.');
			
			return $this->redirectToRoute('filmotheque_acteur_lister');
		}
		
		return $this->render('@Filmotheque/Acteur/ajouter.html.twig', array('formActeur' => $formActeur->CreateView ()));
	}
	
	public function modifierAction($id) {
		return $this->render('@Filmotheque/Acteur/modifier.html.twig');
	}*/
	

 
    public function supprimerAction($id){
    
      $em = $this->getDoctrine()->getManager();
      $acteur = $em->find('FilmothequeBundle:Acteur',$id);
      if(!$acteur){
        throw new NotFoundHttpException("Acteur non trouve");
      }
      $em->remove($acteur);
      $em->flush();
      return $this->redirectToRoute('filmotheque_acteur_lister');
    
    }
	
	public function editerAction(Request $request, $id=null) {
		$em  = $this->getDoctrine()->getManager();
		
		if (isset($id)) {
			$acteur = $em->find('FilmothequeBundle:Acteur', $id);
		} else {
			$acteur = new Acteur();
		}
		
		$formActeur = $this->createForm(ActeurType::class, $acteur);
		
		if ($request->isMethod('POST')  && $formActeur->handleRequest($request)->isValid()) {
			$em->persist($acteur);
			$em->flush();
			
			$request->getSession()->getFlashBag()->add('notice', 'Acteur enregistre.');
      
      $message = $this->get('translator')->trans('acteur.ajouter.succes',array(
                 '%nom%'=>$acteur->getNom(),
                 '%prenom%'=>$acteur->getPrenom()
      ));
      $request->getSession()->getFlashBag()->add('notice',$message);
			
			return $this->redirectToRoute('filmotheque_acteur_lister');
		}
		
		return $this->render('@Filmotheque/Acteur/editer.html.twig', array('formActeur' => $formActeur->CreateView ()));
	}
   
	public function topAction($max=5){
		$em=$this->getDoctrine()->getManager();
		$qb=$em->createQueryBuilder();
		$qb->select('a')
			->from('FilmothequeBundle:Acteur','a')
			->orderBy('a.dateNaissance', 'DESC')
			->setMaxResults($max);
		$query=$qb->getQuery();
		$acteurs=$query->getResult();
		return $this->render('@Filmotheque/Acteur/liste.html.twig',array('acteurs'=>$acteurs));
	}

	public function rechercherAction(Request $request) {
        // si la méthode AJAX a été utilisée
        if($request->isXmlHttpRequest()) {
            $motcle = '';
            // on récupère le contenu du champs motclé
            $motcle = $request->request->get('motcle');
            // on récupère le gestionnaire d’entité
            $em = $this->getDoctrine()->getManager();
            // si le mot-clé existe�?
            if($motcle != '') {
                $qb = $em->createQueryBuilder();
                // on définit une requête préparée qui va rechercher les acteurs dont le nom ou le 
                // prénom ressemble au mot-clé saisi
                $qb->select('a')
                    ->from('FilmothequeBundle:Acteur', 'a')
                    ->where("a.nom LIKE :motcle OR a.prenom LIKE :motcle")
                    ->orderBy('a.nom', 'ASC') 
                    ->setParameter('motcle', '%'.$motcle.'%');
                $query = $qb->getQuery();
                $acteurs = $query->getResult();
            }
            // sinon on récupère tous les acteurs
            else {
                $acteurs = $em->getRepository('FilmothequeBundle:Acteur')->findAll();
            }
            // on retourne la liste des acteurs trouvés dans le formulaire liste.html.twig
            return $this->render('@Filmotheque/Acteur/liste.html.twig',
                                    array('acteurs' => $acteurs, 
                             ));                          
        }
        // si la méthode AJAX n’a pas été appelée alors on renvoie vers la méthode listerAction
        else {
            return $this->listerAction();
        }
    }

}
