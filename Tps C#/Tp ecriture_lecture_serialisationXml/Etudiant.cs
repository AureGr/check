﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace cTp6
{
    public class Etudiant 
    {
        private int numero;
        private DateTime dateDeNaissance;
        private string nom;
        private string prenom;


       public Etudiant() { }

        

        public Etudiant(int numero, DateTime dateDeNaissance, string nom, string prenom)
        {
            this.numero = numero;
            this.dateDeNaissance = dateDeNaissance;
            this.nom = nom;
            this.prenom = prenom;
        }

        public int Numero{ get { return numero; } set { numero=value; } }
        
        public DateTime DateDeNaissance { get { return dateDeNaissance; } set { dateDeNaissance = value; } }
        public string Nom { get { return nom; } set { nom = value; } }
        public string Prenom { get { return prenom; } set { prenom = value; } }

        public void affiche()
        {
            Console.WriteLine("Etudiant numéro: " + numero+ " ;Date de naissance: "+ dateDeNaissance.ToShortDateString());    
        }
    }
}
