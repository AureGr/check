package base;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.Timer;

@SuppressWarnings("serial")
public class panelMenu extends JPanel {
	private JLabel labScore 			= new JLabel("0");
	private JButton retry				= new JButton("RETRY"), nouv=new JButton("new Game");
	private JRadioButton coup				= new JRadioButton("Coups"),	time=new JRadioButton("Temps");
	private String[] allPaire			= {"2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
	private ButtonGroup	grp				= new ButtonGroup();
	private FramePrincipale theFrame;
	private boolean modeCoup            = true;
	private int temps					=0;
	private boolean partieLancee		=false;
	private Timer timer;
	private Font Arial3 				= new Font("Arial",Font.BOLD,28);
	
	private JPanel gridCentre 			= new JPanel();
	private JPanel flowSud 				= new JPanel();

	private JPanel petitFlow1 			= new JPanel();
	private JPanel petitflow2 			= new JPanel();
	

	
	public panelMenu(FramePrincipale pfFrame) {
		super();
		this.setLayout(new BorderLayout());
		theFrame=pfFrame;
		
		timer = new Timer(1000, new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					labScore.setText(""+Integer.toString(temps)+" secondes");
					if(theFrame.getJeu().isPartieTerminee() ) {
						((Timer) (e.getSource())).stop();
					}
					temps++;
				} });
		
		
		

		JComboBox paireList = new JComboBox(allPaire);

		gridCentre.setLayout(new GridLayout(3, 1));
		flowSud.setLayout(new FlowLayout());
		petitFlow1.setLayout(new FlowLayout());
		petitflow2.setLayout(new FlowLayout());
		paireList.setSelectedIndex(6);
		labScore.setHorizontalAlignment(JLabel.CENTER);
		labScore.setFont(Arial3);
		
		coup.setSelected(true);


		this.add(gridCentre, BorderLayout.CENTER);
		this.add(flowSud, BorderLayout.SOUTH);
		gridCentre.add(petitFlow1);
		gridCentre.add(labScore);
		gridCentre.add(petitflow2);
		flowSud.add(retry);
		flowSud.add(nouv);
		petitFlow1.add(new JLabel("Mode de jeu :"));
		petitFlow1.add(coup);
		petitFlow1.add(time);
		grp.add(coup);
		grp.add(time);
		petitflow2.add(paireList);
		petitflow2.add(new JLabel("paires"));

		retry.addActionListener((ActionEvent e) -> retry());
		nouv.addActionListener((ActionEvent e) -> newGame());
		coup.addActionListener((ActionEvent e)-> modeCoup());
		time.addActionListener((ActionEvent e)-> modeTime());
		paireList.addActionListener((ActionEvent e) -> setNbPaires(Integer.parseInt((String)paireList.getSelectedItem())));
		
	}
	
	public void setNbPaires(int nbPaires) {
		timer.stop();
		partieLancee=false;
		theFrame.setNbPaires(nbPaires);
		theFrame.newGame();
	}
	
	
	public void retry() {
		timer.stop();
		partieLancee=false;
		theFrame.retry();
		
		
	}
	public void newGame() {
		timer.stop();
		partieLancee=false;
		theFrame.newGame();
		
		
	}
	

	public void modeCoup() {
		timer.stop();
		partieLancee=false;
		modeCoup=true;
		theFrame.newGame();
		
		
		
	}

	public void modeTime() {
		partieLancee=false;
		modeCoup=false;
		theFrame.newGame();
		
	}

	public void setScore(int pfNbCoups) {
		if (modeCoup) {
			labScore.setText(""+pfNbCoups);
		}
		else {
			if(!partieLancee) {
				labScore.setText(""+pfNbCoups);
			}
			
			if (!partieLancee && theFrame.isPartieLancee()) {
				temps=0;
				timer.setInitialDelay(0);
				timer.start();
				partieLancee=true;
			}
			


		}
	}
}


