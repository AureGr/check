package annuaire.iu;

import annuaire.om.Annuaire;
import annuaire.om.Personne;

public class TestSauvegarde {
	public static String NOM_FIC = "listePersonne.sav";
	public static String NOM_FIC_Annuaire = "annuaire.sav";
	public static void main(String[] args) {
		Personne   personne;
		Annuaire   annuaire;
		
		annuaire = new Annuaire("Gens Sauvés");
		
		personne = new Personne("DUJARDIN","Jean", 42);
		annuaire.add(personne);
		personne = new Personne("DURAND","Andy", 24);
		annuaire.add(personne);
		personne = new Personne("MOULIN","Menier", 50);
		annuaire.add(personne);
		personne = new Personne("SVD","Andy", 27);
		annuaire.add(personne);
		personne = new Personne("O'BRIAN","Jack", 22);
		annuaire.add(personne);
		
		//annuaire.writePersonne(NOM_FIC);
		annuaire.writeAnnuaire(NOM_FIC_Annuaire);

	}

}
