package annuaire.iu;

import java.util.Enumeration;

import annuaire.om.Annuaire;
import annuaire.om.Personne;

public class TestRestoration {
	public static String NOM_FIC = "listePersonne.sav";
	public static String NOM_FIC_Annuaire = "annuaire.sav";

	public static void main(String[] args) {
		Annuaire   annuaire;
			
		annuaire = new Annuaire(" rest Gens Sauvés");
	
		//annuaire.readPersonne(NOM_FIC);
		annuaire.readAnnuaire(NOM_FIC_Annuaire);
		afficherAnnuaire(annuaire);
		
		
		System.out.println("=======================================================");
		System.out.println("Personnes ayant le prénom ''Andy'' :");
		for ( Enumeration<Personne> enuPersonne = annuaire.getPersonnesFromPrenom("Andy"); enuPersonne.hasMoreElements(); ) {
			System.out.println( enuPersonne.nextElement().toString() );
		}
	}
	
	
public static void afficherAnnuaire(Annuaire annuaire) {
		
		Enumeration<Personne>   enuPersonne = annuaire.getPersonnes();
		
		System.out.println("------------------------------------------------------");
		System.out.println("l'annuaire ["+annuaire.getNom()+"] contient "+annuaire.size()+" personne(s)");
		System.out.println("------------------------------------------------------");
		while (enuPersonne.hasMoreElements()) {
			System.out.println( enuPersonne.nextElement().toString() );
		}
	}

}

