﻿using System;
using System.Collections.Generic;
using System.Text;

namespace cTp4
{
    class Etudiant 
    {
        private int numero;
        private DateTime dateDeNaissance;

        public Etudiant(int numero, DateTime date)
        {
            this.numero = numero;
            this.dateDeNaissance = date;
        }

        public int Numero{ get { return numero; } }
        public DateTime DateDeNaissance { get { return dateDeNaissance; } }

        public void affiche()
        {
            Console.WriteLine("Etudiant numéro: " + numero+ " ;Date de naissance: "+ dateDeNaissance.ToShortDateString());    
        }
    }
}
