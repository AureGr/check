﻿using System;
using System.Collections.Generic;
using System.Text;

namespace cTp5
{
    class Universite
    {
        private List<Etudiant>  listEtudiant = new List<Etudiant>();

        public Universite(List<Etudiant> listE)
        {
            this.listEtudiant = listE;
        }

        public void ajoute(Etudiant E)
        {
            listEtudiant.Add(E);
        }

        public List<Etudiant> ListEtudiant
        {
            get { return listEtudiant; }
        }

        public Etudiant getEtudiant(int num)
        {
            foreach (Etudiant item in listEtudiant)
            {
                if (item.Numero == num) { return item; }
            }
           throw new EtudiantNonExistantException(num);


            
        }

    }
}
