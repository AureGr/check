<?php

namespace FilmothequeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FilmothequeBundle\Entity\Categorie;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller {


    public function indexAction(){
    // on recupere le gestionnaire de donnees
    /*$em = $this->getDoctrine()->getManager();
    $categorie1 = new Categorie();
    $categorie1->setNom('Comedie');
    $em->persist($categorie1);
    $categorie2 = new Categorie();
    $categorie2->setNom('Science-fiction');
    $em->persist($categorie2);
    
    // le flush fait l'equivalent d'un commit
    $em->flush(); */
	
	return $this->render('@Filmotheque/Default/index.html.twig');
	
	/*$em = $this->getDoctrine()->getManager();
	$categories = $em->getRepository('FilmothequeBundle:Categorie')->findAll();
	return $this->render('@Filmotheque/Default/index.html.twig',array('categories'=>$categories));
    */
    }


}
