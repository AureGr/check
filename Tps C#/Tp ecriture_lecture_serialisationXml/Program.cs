﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace cTp6
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Etudiant> listEtu = new List<Etudiant>() { new Etudiant(1, DateTime.Parse("16/05/1964"), "AA", "A"), new Etudiant(2, DateTime.Parse("01/05/1554"), "BB" ,"B"), new Etudiant(3, DateTime.Parse("16/11/1999"), "CC", "C"), new Etudiant(4, DateTime.Parse("20/05/2000"), "DD", "D"), new Etudiant(5, DateTime.Parse("16/09/1800"), "EE", "E"), new Etudiant(6, DateTime.Parse("30/06/2001"), "FF", "F"), new Etudiant(7, DateTime.Parse("16/03/2002"), "GG", "G"), new Etudiant(8, DateTime.Parse("15/01/1964"), "HH", "H"), new Etudiant(9, DateTime.Parse("17/08/1950"), "II", "I"), new Etudiant(10, DateTime.Parse("25/12/1961"), "JJ", "J") };

            //ecriture simple
            StreamWriter flux = null;
            try
            {
                flux = new StreamWriter("test.txt");
                listEtu.ForEach(etu => flux.WriteLine(etu.Nom + " " + etu.Prenom + " : " + etu.Numero));
            }
            catch (Exception e)
            {
                Console.WriteLine("Something heppened : " + e.Message);
            }
            finally
            {
                flux.Close();
            }

            //serialisation xml de la liste
            XmlSerializer xs = new XmlSerializer(typeof(List<Etudiant>));
            StreamWriter wr = new StreamWriter("listetu.xml");
            xs.Serialize(wr, listEtu);
            wr.Close();
            
            //lecture xml
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.Load("listetu.xml");
                XmlElement racine = xmlDoc.DocumentElement;
                XmlNode mod = racine.FirstChild;

                for (int i = racine.ChildNodes.Count - 1; i >= 0; i--)
                {
                    Console.WriteLine(racine.ChildNodes[i].ChildNodes[3].InnerText + " " + racine.ChildNodes[i].ChildNodes[2].InnerText);

                } 
            }
            catch (Exception e)
            {
                Console.WriteLine("Probleme a la lecture du fichier xml : " + e.Message);
            }

            Console.Read();        
        }
    }
}
