<?php

namespace FilmothequeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FilmothequeBundle\Form\CategorieType;
use Symfony\Component\HttpFoundation\Request;

use FilmothequeBundle\Entity\Categorie;

class CategorieController extends Controller {
	
	public function listerAction(){
   
   $em = $this->getDoctrine()->getManager();
   $categories = $em->getRepository('FilmothequeBundle:Categorie')->findAll();
   return $this->render('@Filmotheque/Categorie/lister.html.twig',array('categories' =>$categories));
   
 }
 
    public function supprimerAction($id){
    
      $em = $this->getDoctrine()->getManager();
      $categorie = $em->find('FilmothequeBundle:Categorie',$id);
      if(!$categorie){
        throw new NotFoundHttpException("Categorie non trouve");
      }
      $em->remove($categorie);
      $em->flush();
      return $this->redirectToRoute('filmotheque_categorie_lister');
    
    }
	
	public function editerAction(Request $request, $id=null) {
		$em  = $this->getDoctrine()->getManager();
		
		if (isset($id)) {
			$categorie = $em->find('FilmothequeBundle:Categorie', $id);
		} else {
			$categorie = new Categorie();
		}
		
		$formCategorie = $this->createForm(CategorieType::class, $categorie);
		
		if ($request->isMethod('POST')  && $formCategorie->handleRequest($request)->isValid()) {
			$em->persist($categorie);
			$em->flush();
			
			$request->getSession()->getFlashBag()->add('notice', 'Categorie enregistre.');
			
			return $this->redirectToRoute('filmotheque_categorie_lister');
		}
		
		return $this->render('@Filmotheque/Categorie/editer.html.twig', array('formCategorie' => $formCategorie->CreateView ()));
	}
   
 
}
